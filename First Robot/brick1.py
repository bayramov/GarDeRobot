#!/usr/bin/env python3
from ev3dev.ev3 import *
from motors import *

cs1 = ColorSensor('in2') 
assert cs1.connected, "Connect a touch sensor to sensor port 2"

cs2 = ColorSensor('in4')
assert cs2.connected, "Connect a touch sensor to sensor port 4"

cs1.mode='COL-REFLECT'
cs2.mode='COL-REFLECT'
desiredValue = 30

while True:
    if cs1.value > desiredValue:
        Motors.straight1()
    else:
        if cs2.value > desiredValue:
            Motors.straight2()
        else:
            Motors.straight3()