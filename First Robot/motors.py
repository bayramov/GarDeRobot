#!/usr/bin/env python3
from ev3dev.ev3 import *

mA = LargeMotor('outA')
mB = LargeMotor('outB')
mC = LargeMotor('outC')
mD = LargeMotor('outD')

class Motors:
    def straight1():
        mA.run_forever(speed_sp=100)
        mB.run_forever(speed_sp=-1000)
        mC.run_forever(speed_sp=100)
        mD.run_forever(speed_sp=-1000)
    def straight2():
        mA.run_forever(speed_sp=-1000)
        mB.run_forever(speed_sp=100)
        mC.run_forever(speed_sp=-1000)
        mD.run_forever(speed_sp=100)
    def straight3():
        mA.run_forever(speed_sp=-400)
        mB.run_forever(speed_sp=-400)
        mC.run_forever(speed_sp=400)
        mD.run_forever(speed_sp=400)
    def stopAll():
        mA.run_forever(speed_sp=0)
        mB.run_forever(speed_sp=0)
        mC.run_forever(speed_sp=0)
        mD.run_forever(speed_sp=0)
